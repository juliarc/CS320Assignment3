extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
	#include "lua.hpp"
}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char* argv[]) {

	std::cout << "Assignment #3-1, Julia Cassella, juliarc@sandiego.edu\n";

	//read in the command line argument 
	char *filename;
	filename  = argv[1];

	//now create a lua state
	lua_State *state;
	state = luaL_newstate();
	
	//open the libraries in the state
	luaL_openlibs(state);

	//load the lua file (stored in filename)
	if (luaL_loadfilex(state, filename, NULL)) {
	
		std::cout << "error in load\n";
		exit(1);
	
	}   
		     

	
	//run the progam or print off a message saying that it failed
	if (lua_pcall(state, 0, 0, 0)){

		std::cout << "error in execute\n";                 
		exit(1); 
	}	
	//close the lua state
	lua_close(state);
}
