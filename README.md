Julia Cassella
juliarc@sandiego.edu

The first file prog3_1.cpp was written in c++. It's purpose was to read in the name of a lua file from a command line argument, and execute that file. I did this by creating a lua state from the file that was read in.The file was compiled using g++ prog3_1.cpp –o prog31–I lua-5.3.4/src –L lua-5.3.4/src –l lua –l m -l dl

prog3_2.lua was a program written in lua that converted from infix to postfix using a stack. THere were two stacks one contained output and the other contained the operators. All numbers were put directly on the ouput and opperators were put on the opperator as long as the top of the stack had lower precedence then the current. If the top had higher precdence the opperators were popped off and put on the ouput stack until one with lower precedence was reached. Finally after all the input was scanned any remaining tokens on the operators were popped and put on the output
This program had to be run in a lua environment using the dofile("prog3_2.lua") and then the function was called

prog3_3.cpp this read in the lua file from the command line argument and then read an input string from the command line. Then the InfixToPostFix program was loaded onto the stak and the argument was loaded onto the stack as well. Finally a p call ran the program and the result was retreieved from the stack and printed off. You compile with the needed lua file just like it was compiled in the first one. g++ prog3_1.cppg++ prog3_1.cpp –o prog31–I lua-5.3.4/src –L lua-5.3.4/src –l lua –l m -l dl 
