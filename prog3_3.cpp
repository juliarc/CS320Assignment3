extern "C" {
	#include "lua.h"
        #include "lualib.h"
        #include "lauxlib.h"
        #include "lua.hpp"

}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;



int main(int argc, char* argv[]) {

        std::cout << "Assignment #3-3, Julia Cassella, juliarc@sandiego.edu\n";

        //read in the command line argument 
        char *filename;
        filename  = argv[1];
			
	string input; 
	getline (cin,input);
	
        //now create a lua state
        lua_State *state;
        state = luaL_newstate();

        //open the libraries in the state
        luaL_openlibs(state);
	
	
        //load the lua file (stored in filename)
        if (luaL_loadfilex(state, filename, NULL)) {

                std::cout << "error in load\n";
                exit(1);

        }

	//get the function and then prime with an empty pcall

	lua_pcall(state, 0, 0, 0);

	lua_getglobal(state, "InfixToPostfix");
	
	

	lua_pushstring(state, input.c_str());   /* push 1st argument */
	
 
	/* do the call (1 arguments, 1 result) */
	if (lua_pcall(state, 1, 1, 0) != 0) {
		printf("error running function `f': %s\n",lua_tostring(state, -1));
		return -1;
	}
 

	//get the return value off the stack and print it 
 	std::cout << lua_tolstring(state, -1, NULL);

	lua_pop(state, 1);	

	
        //close the lua state
        lua_close(state);
}

